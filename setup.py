"""
revit-py setup
"""

from distutils.core import setup

setup(
      name='wver',
      packages=['wver'],
      version='0.2',
      description='Cli interface for rev, online code review tool',
      author='brwnrclse (Barry Harris)',
      author_email='dev@vaemoi.co',
      url='https://rev.vaemoi.co',
      download_url='https://gitlab.com/vaemoi/wver/wver-cli-python/repository/master/archive.tar.gz',
      keywords=['code review', 'wver', 'oss'],
      classifiers=[],
)
